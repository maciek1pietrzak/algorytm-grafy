package main

import (
	"fmt"
	"bufio"
	"log"
	"strings"
	"strconv"
	"os"
)

const WHITE int = 0;

const GREY int = 1;

const BLACK int = 2;

type Verticle struct {
	edges []int;
	id    int;
	color int;
	start int;
	end   int;
}

var time = 0;

var verticles []*Verticle

var list = make([]Verticle, 0)

func init() {
	file, err := os.Open("/run/media/maciok/user-files/pja/grafy/algorytm/b.resource")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Scan()
	initVal := strings.Split(scanner.Text(), " ")
	size, _ := strconv.Atoi(initVal[0])
	verticles = make([]*Verticle, size)
	for i := range verticles {
		verticles[i] = new(Verticle)
		verticles[i].id = i
		verticles[i].color = WHITE
		verticles[i].end = -1
		verticles[i].start = -1
		verticles[i].edges = make([]int, 0)
	}

	for scanner.Scan() {
		str := scanner.Text()
		//fmt.Println(str)
		v := strings.Split(str, " ")
		v1, _ := strconv.Atoi(v[0])
		v2, _ := strconv.Atoi(v[1])
		verticles[v1].edges = append(verticles[v1].edges, v2)
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
}

func main() {
	for _, v := range verticles {
		if (v.color == WHITE) {
			recursiveDFS(v)
		}
	}

	for _, v2 := range list {
		fmt.Printf("%d\n", v2.id);
	}
}

func recursiveDFS(v *Verticle) {
	v.color = GREY
	v.start = time
	time++
	for _, i := range v.edges {
		if verticles[i].color == WHITE {
			recursiveDFS(verticles[i])
		} else if verticles[i].color == GREY {
			fmt.Println("Jest pętla")
			os.Exit(1);
		}
	}
	v.color = BLACK
	v.end = time
	list = append(list, *v)
	time++
}